// pour la version où on met le mot hashé
const hashcode = process.argv[2]; // node script.js "ZWpxZQ=="

// pour la version où on met le mot à convertir
// const str = process.argv[2]; // node script.js "choc"

const calculateHash = (str) => {
  let hash = str
    .split("")
    .map((c, i) => str.charCodeAt(i))
    .map((c) => c + 2)
    .map((c) => String.fromCharCode(c))
    .join("");

  // pour la version où on met le mot à convertir
  // const hashcode = Buffer.from(hash).toString("base64");
  // console.log(hashcode); // ZWpxZQ==

  const convertedHashcode = Buffer.from(hashcode, "base64").toString("ascii"); // ejqe
  // console.log(convertedHashcode); // ejqe

  const code = convertedHashcode
    .split("")
    .map((e, i) => convertedHashcode.charCodeAt(i))
    .map((e) => e - 2)
    .map((e) => String.fromCharCode(e))
    .join("");
  console.log(code); // choc

  return Buffer.from(hash).toString("base64");
};

// pour la version où on met le mot hashé
calculateHash("");

// pour la version où on met le mot à convertir
// calculateHash(str);
